﻿using VacationSchedule.Core.Contracts;
using VacationSchedule.WebApp.Server.RabbitMQProducers;

namespace VacationSchedule.WebApp.Server.Notification
{
    public class NotificationMail : INotification
    {
        private readonly ISendEmailRabbitProducer _sendEmailRabbitMQProducer;

        public NotificationMail(ISendEmailRabbitProducer sendEmailRabbitMQProducer)
        {
            _sendEmailRabbitMQProducer = sendEmailRabbitMQProducer;
        }

        public void Send(IEnumerable<string> address, string subject, string message)
        {
            _sendEmailRabbitMQProducer.Send(new EmailContract()
            {
                MailTo = string.Join("; ", address),
                Subject = subject,
                Message = message
            }
            );
        }
    }
}
