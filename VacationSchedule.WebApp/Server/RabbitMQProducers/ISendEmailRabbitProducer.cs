﻿using VacationSchedule.Core.Contracts;

namespace VacationSchedule.WebApp.Server.RabbitMQProducers
{
    public interface ISendEmailRabbitProducer
    {
        Task Send<IEmailContract>(IEmailContract message);
    }
}
