﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public UserController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return _db.Users;
        }

        // GET api/<ValuesController>/5
        [HttpGet("GetByUserName")]
        public User? GetByUserName(string userName)
        {
            return _db.Users.Where(x => x.Login == userName).FirstOrDefault();
        }

        // GET api/<ValuesController>/5
        [HttpGet("GetByDepartmentId")]
        public IEnumerable<User> GetByDepartmentId(int id)
        {
            return _db.Users.Where(x => x.DepartmentId == id);
        }

        [HttpGet("GetByUserId")]
        public User? GetByUserId(int id)
        {
            return _db.Users.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] User value)
        {
            if (value.Id == 0)
            {
                value.RoleId = _db.Role.Where(x => x.RoleName == "Basic").FirstOrDefault().Id;
                value.DepartmentId = _db.Departments.Where(x => x.DepartmentName == "Неизвестно").FirstOrDefault().Id;
                value.Department = null;
                _db.Users.Add(value);
                _db.SaveChanges();
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] User value)
        {
            var existUser = _db.Users.Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            if (existUser is not null)
            {
                var unknownRole = _db.Role.Where(x => x.RoleName == "Unknown").FirstOrDefault()?.Id;
                if (existUser.RoleId == unknownRole && value.RoleId != unknownRole)
                {
                    //TODO: Команда модератору, о новом пользователе
                }
                _db.Users.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
