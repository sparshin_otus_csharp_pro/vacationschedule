﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public LogController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<Log> Get()
        {
            return _db.Logs;
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public Log? Get(int id)
        {
            return _db.Logs.Where(x => x.Id == id).FirstOrDefault();
        }
    }
}
