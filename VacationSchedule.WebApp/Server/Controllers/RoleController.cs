﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public RoleController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<RoleController>
        [HttpGet]
        public IEnumerable<Role> Get()
        {
            return _db.Role;
        }

        // GET api/<RoleController>/5
        [HttpGet("{id}")]
        public Role? Get(int id)
        {
            return _db.Role.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST api/<RoleController>
        [HttpPost]
        public void Post([FromBody] Role value)
        {
            if (value.Id == 0)
            {
                _db.Role.Add(value);
                _db.SaveChanges();
            }
        }

        // PUT api/<RoleController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Role value)
        {
            if (_db.Role.Where(x => x.Id == id).AsNoTracking().FirstOrDefault() is not null)
            {
                _db.Role.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<RoleController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
