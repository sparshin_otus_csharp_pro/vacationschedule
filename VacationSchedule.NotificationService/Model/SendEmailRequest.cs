﻿namespace VacationSchedule.NotificationService.Model
{
    public class SendEmailRequest
    {
        public string MailTo { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
