﻿namespace VacationSchedule.Core.Models.DB
{
    public class UserContact
    {
        public int Id { get; set; }
        public string? Contact { get; set; }

        public int UserId { get; set; }
        public User? User { get; set; }

        public int TypeContactId { get; set; }
        public ContactType? TypeContact { get; set; }
    }
}
