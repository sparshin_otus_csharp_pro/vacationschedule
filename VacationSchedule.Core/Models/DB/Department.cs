﻿namespace VacationSchedule.Core.Models.DB
{
    public class Department
    {
        public int Id { get; set; }
        public string? DepartmentName { get; set; }

        public int? ParentId { get; set; }
        public Department? Parent { get; set; }
        public List<Department>? SubDepartments { get; set; }
        public User? HeadOfDepartmentUser { get; set; }
        public List<User>? Users { get; set; }

        public Department()
        {
            SubDepartments = new List<Department>();
            Users = new List<User>();
        }
    }
}
